This repository contains the R code associated with the paper:

Gene co-expression modules underlying polymorphic and monomorphic zooids in the colonial hydrozoan, Hydractinia symbiolongicarpus

David C. Plachetzki, M. Sabrina Pankey, Brian R. Johnson, Eric J. Ronne, Artyom Kopp and Richard K. Grosberg.

To be published in Integrative and Comparative Biology



To run this script, 

1. download the repository and the data files from the downloads section

2. unzip and place R script and data files in directory of your choice

3. change your working directory in R to that directory

4. execute script in R



Additional data matrices and sequence files associated with this paper are accessioned at dryad:

doi:10.5061/dryad.98pt3